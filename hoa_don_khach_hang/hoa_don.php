<?php 
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan_khach_hang = $_SESSION["tai_khoan_khach_hang"];
		$ma_khach_hang = $_SESSION["ma_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Hoa don khach hang</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>
	<?php
		include('../template_webbanhang/template_upper_part.php');
		
		// lay thong tin hoa don ve
		$lenh = mysqli_query($ket_noi,"select * from hoa_don where ma_khach_hang=$ma_khach_hang order by ma_hoa_don desc");
	?>

	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
				Hoa don
			</h1>
		</a>
	</div>

<?php
	$kiem_tra_lenh = mysqli_num_rows($lenh);
	if($kiem_tra_lenh != 0){
?>

	<table>
		<tr>
			<td>
				<div class="form_hoa_don">
					<label for="ten_nguoi_nhan">
						Ten nguoi nhan
					</label>
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					<label for="so_dien_thoai_nguoi_nhan">
						So dien thoai nguoi nhan
					</label>
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					<label for="dia_chi_nguoi_nhan">
						Dia chi nguoi nhan
					</label>
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					<label for="ngay_dat_hang">
							Ngay dat hang
					</label>
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					<label for="tinh_trang_giao_hang">
						Tinh trang giao hang
					</label>
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					<label for="thanh_tien">
						Tong tien
					</label>
				</div>
			</td>
		</tr>

		<?php 
			while($hoa_don = mysqli_fetch_array($lenh)){
		?>
		<tr>

		<form action="cap_nhat_don_hang.php">

			<td>
				<div class="input_hoa_don">
					<input type="text" name="ten_nguoi_nhan" id="ten_nguoi_nhan" value="<?php echo $hoa_don["ten_nguoi_nhan"] ?>">
					<div id="loi_ten_nguoi_nhan"></div>
				</div>
			</td>
			<td>
				<div class="input_hoa_don">
					<input type="text" name="so_dien_thoai_nguoi_nhan" id="so_dien_thoai_nguoi_nhan" value="<?php echo $hoa_don["so_dien_thoai_nguoi_nhan"] ?>">
					<div id="loi_so_dien_thoai_nguoi_nhan"></div>
				</div>
			</td>
			<td>
				<div class="input_hoa_don">
					<input type="text" name="dia_chi_nguoi_nhan" id="dia_chi_nguoi_nhan" value="<?php echo $hoa_don["dia_chi_nguoi_nhan"] ?>">
					<div id="loi_dia_chi_nguoi_nhan"></div>
				</div>
			</td>
			<td>
				<div class="input_hoa_don">
					<input type="text" id="ngay_dat_hang" value="<?php $ngay_dat_hang = $hoa_don["ngay_dat_hang"];
echo date("j/m/Y", strtotime($ngay_dat_hang)); ?>" readonly>
				</div>
			</td>
			<td>
				<div class="input_hoa_don">
					<input type="text" id="tinh_trang_giao_hang" value="<?php if($hoa_don["tinh_trang_giao_hang"]==0){
							echo "Chua giao hang";
						}else if($hoa_don["tinh_trang_giao_hang"]==1){
							echo "Dang giao hang";
						}else{
							echo "Da giao hang";
						} ?>" readonly>
				</div>
			</td>
			<td>
				<div class="input_hoa_don">
					<input type="text" id="thanh_tien" value="<?php echo $hoa_don["thanh_tien"] ?>" readonly>
				</div>
			</td>
			<td>
				<input type="hidden" name="ma_hoa_don" value="<?php echo $hoa_don["ma_hoa_don"] ?>">
				<div class="dieu_huong_hoa_don">
					<button type="submit" id="cap_nhat_don_hang" onclick="return kiem_tra_don_hang()">
						Cap nhat don hang
					</button>
				</div>
			</td>

		</form>

			<td>
				<div class="dieu_huong_hoa_don">
					<a href="xem_chi_tiet_hoa_don.php?ma_hoa_don=<?php echo $hoa_don["ma_hoa_don"] ?>">
						<img src="../images/icon_detail.jpg" width="35px" height="35px">
					</a>
				</div>
			</td>
			<td>
				<div class="dieu_huong_hoa_don">
					<a href="xoa_don_hang.php?ma_hoa_don=<?php echo $hoa_don["ma_hoa_don"] ?>">
						<img src="../images/icon_delete.jpg" width="35px" height="35px">
					</a>
				</div>
			</td>
		</tr>

		<?php
		// ngoac cua vong lap while
		}
		?>
	</table>
	
	<script type="text/javascript">
		function kiem_tra_don_hang(){
			var dem_loi_hoa_don = 0;

			var ten_nguoi_nhan = document.getElementById("ten_nguoi_nhan").value;
			var regex_ten_nguoi_nhan = /^[a-z0-9\_\-\s]+$/i;
			var kiem_tra_ten_nguoi_nhan = regex_ten_nguoi_nhan.test(ten_nguoi_nhan);
			if(ten_nguoi_nhan.length == 0){
				document.getElementById("loi_ten_nguoi_nhan").innerHTML = 'Khong duoc de trong';
				dem_loi_hoa_don = 1;
			}else {
				if(kiem_tra_ten_nguoi_nhan == false){
					document.getElementById("loi_ten_nguoi_nhan").innerHTML = 'Nhap sai dinh dang';
					dem_loi_hoa_don = 1;
				}else{
					document.getElementById("loi_ten_nguoi_nhan").innerHTML = '';
				}
			}

			var so_dien_thoai_nguoi_nhan = document.getElementById("so_dien_thoai_nguoi_nhan").value;
			var regex_so_dien_thoai_nguoi_nhan = /^[\+]?[0-9]{10,12}$/;
			var kiem_tra_so_dien_thoai_nguoi_nhan = regex_so_dien_thoai_nguoi_nhan.test(so_dien_thoai_nguoi_nhan);
			if(so_dien_thoai_nguoi_nhan.length == 0){
				document.getElementById("loi_so_dien_thoai_nguoi_nhan").innerHTML = 'Khong duoc de trong';
				dem_loi_hoa_don = 1;
			}else {
				if(kiem_tra_so_dien_thoai_nguoi_nhan == false){
					document.getElementById("loi_so_dien_thoai_nguoi_nhan").innerHTML = 'Nhap sai dinh dang';
					dem_loi_hoa_don = 1;
				}else{
					document.getElementById("loi_so_dien_thoai_nguoi_nhan").innerHTML = '';
				}
			}

			var dia_chi_nguoi_nhan = document.getElementById("dia_chi_nguoi_nhan").value;
			var regex_dia_chi_nguoi_nhan = /^[a-z0-9]+[\,\. a-z0-9]*[a-z0-9]+$/i;
			var kiem_tra_dia_chi_nguoi_nhan = regex_dia_chi_nguoi_nhan.test(dia_chi_nguoi_nhan);
			if(dia_chi_nguoi_nhan.length == 0){
				document.getElementById("loi_dia_chi_nguoi_nhan").innerHTML = 'Khong duoc de trong';
				dem_loi_hoa_don = 1;
			}else {
				if(kiem_tra_dia_chi_nguoi_nhan == false){
					document.getElementById("loi_dia_chi_nguoi_nhan").innerHTML = 'Nhap sai dinh dang';
					dem_loi_hoa_don = 1;
				}else{
					document.getElementById("loi_dia_chi_nguoi_nhan").innerHTML = '';
				}
			}

			// Hien thi loi
			if(dem_loi_hoa_don == 1){
				return false;
			}else {
				document.getElementById("cap_nhat_don_hang").submit();
			}
		}
	</script>

<?php
	}else{
		echo "<h1>Khong co hoa don. Nhan nut quay lai de tiep tuc mua sam</h1>";
	}
?>

	<div class="icon_come_back">
		<a href="../tai_khoan_khach_hang/quan_ly_tai_khoan.php" title="Quay lai trang quan ly tai khoan">
			<img src="../images/icon_come_back.jpg" width="35px" height="35px">
		</a>
	</div>

</body>
</html>
<?php
		include('../connecting/close.php');
		
	}else{
		header("location:../login_khach_hang/login_khach_hang.php");
	}
?>