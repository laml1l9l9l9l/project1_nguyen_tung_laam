<?php 
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan_khach_hang = $_SESSION["tai_khoan_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Xem chi tiet hoa don</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>
	<?php
	if(isset($_GET["ma_hoa_don"])){
		$ma_hoa_don = $_GET["ma_hoa_don"];
		include('../template_webbanhang/template_upper_part.php');
		
		// lay thong tin hoa don ve
		$lenh = mysqli_query($ket_noi,"select hoa_don_chi_tiet.so_luong, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham from (hoa_don inner join (hoa_don_chi_tiet inner join san_pham on hoa_don_chi_tiet.ma_san_pham = san_pham.ma_san_pham) on hoa_don_chi_tiet.ma_hoa_don = hoa_don.ma_hoa_don) where hoa_don_chi_tiet.ma_hoa_don = $ma_hoa_don");

		include '../connecting/close.php';
	?>
	
	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
				Xem chi tiet hoa don
			</h1>
		</a>
	</div>

	<table>
		<tr>
			<td>
				<div class="form_hoa_don">
					Anh san pham
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					Ten san pham
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					So luong
				</div>
			</td>
			<td>
				<div class="form_hoa_don">
					Thanh tien
				</div>
			</td>
			<td></td>
		</tr>

		<?php while($hoa_don = mysqli_fetch_array($lenh)){ ?>

		<tr>
			<td>
				<img src="../images/<?php echo $hoa_don["anh_san_pham"] ?>" width="60px" height="80px">
			</td>
			<td>
				<div class="input_hoa_don">
					<?php echo $hoa_don["ten_san_pham"]; ?>
				</div>
			</td>
			<td>
				<div class="input_hoa_don">
					<?php echo $hoa_don["so_luong"] ?>
				</div>
			</td>
			<td>
				<div class="input_hoa_don">
					<?php
						$tong_tien = $hoa_don["gia_san_pham"] * $hoa_don["so_luong"];
						echo $tong_tien;
					?>
				</div>
			</td>
		</tr>

		<?php } ?>

	</table>

	<div class="icon_come_back">
		<a href="hoa_don.php" title="Quay lai trang hoa don">
			<img src="../images/icon_come_back.jpg" width="35px" height="35px">
		</a>
	</div>

	<?php
	// ngoac neu ton tai ma_hoa_don
	}else{
		header("location:hoa_don.php");
	}
	?>
</body>
</html>
<?php
	}else{
		header("location:../login_khach_hang/login_khach_hang.php");
	}
?>