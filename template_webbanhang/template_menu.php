<div id="menu">
<?php 
	include 'template_top.php';
?>	
	<nav id="menu_left">
		<ul>
			<li class="menu_home"><a href="../trang_chu/trang_chu.php" title="Nhan de toi trang chu">Trang chủ</a></li>
			<li><a href="../san_pham/san_pham.php" title="Nhan de toi trang san pham">Sản phẩm</a>
				<ul style="width: 220px;">
					<li style="width: auto;"><a href="../san_pham/san_pham.php?tim_kiem=ao" title="Nhan de toi trang san pham la ao">Áo</a></li>
					<li style="width: auto;"><a href="../san_pham/san_pham.php?tim_kiem=quan" title="Nhan de toi trang san pham la quan">Quần</a></li>
				</ul>
			</li>
			<li><a href="../gioi_thieu/gioi_thieu.php" title="Nhan de toi trang gioi thieu">Giới thiệu</a></li>
			<li><a href="../gio_hang/xem_gio_hang.php" title="Nhan de toi trang gio hang">Giỏ hàng</a></li>
		</ul>
	</nav>
	
	<?php
		include("template_menu_right.php");
	?>

	<!-- icon -->
	<a href="../trang_chu/trang_chu.php" class="anh_icon hover-target" target=”_blank”></a>
</div>