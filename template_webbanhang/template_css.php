<link rel="shortcut icon" href="../images/icon_project1.jpg" type="image/png">
<style>
	body{
		margin-top: 0px;
		margin-left: 1px;
		background: url(../images/background_img.jpg);
		opacity: 0.95;
		filter: alpha(opacity=50);
		background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
	}

	* {
		box-sizing: border-box;
	}

	/*css upper part*/
	#upper_part{
		width: 100%;
		height: 100px;
		position: relative;
	}

	/*css top*/
	#top{
		width: 100%;
		height: 3%;
		position: top;
		text-align: right;
		background:#3dadff;
		float: left;
		position: fixed;
	}
	.span_top{
		padding-right: 20px;
	}
	.span_top a{
		transition: 0.6s;
	}
	.span_top a:hover{
		background:#d1e854
	}
	.top_the_a{
		text-decoration:none;
		color: white;
	}
	
	/*css menu*/
	#menu{
		width: 100%;
		height: 11%;
		background-color: #ffc04d;
		position: fixed;
		z-index: 3;
		box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.5);
	}
	#menu_left{
		margin-top:10px
	}
	#menu_left ul{
		width: 900px;
		list-style:none;
		position:relative;
		float:left;
		margin:7px;
		margin-left: 0px;
		padding:5px
	}
	#menu_left ul a{
		display:block;
		color:#333;
		text-decoration:none;
		font-weight:700;
		font-size:20px;
		line-height:32px;
		padding:0 15px;
		margin-left: 10px;
		padding-right: 0;
		width: 190px;
		height: 50px;
		font-family:"HelveticaNeue","Helvetica Neue",Helvetica,Arial,sans-serif
	}
	#menu_left ul li{
		width: 220px;
		position:relative;
		float:left;
		margin-top:-1px;
		padding-top:10px
	}
	#menu_left ul li.menu_home{
		background:none
	}
	#menu_left ul li:hover{
		width: 220px;
		box-shadow: 10px 10px 5px rgba(0, 0, 0, 0.7);
		background:#d1e854
	}
	#menu_left ul ul{
		display:none;
		position:absolute;
		width:220px;
		top:100%;
		left:0;
		background:#ffc04d;
		margin-top: 0;
		padding-top:-20px
		z-index:0;
	}
	#menu_left ul ul li{
		float:none;
		margin-top: 0px;
		width:auto;
	}
	#menu_left ul ul a{
		line-height:120%;
		padding:10px 20px
	}
	#menu_left ul li:hover > ul{
		width: 210px;
		display:block
	}

	/*css tim kiem theo ten*/
	#menu_right{
		width:25%;
		right:0px;
		padding-top:20px;
		float:right;
	}

	/*css tim kiem theo lua chon*/
	.tim_kiem_theo_lua_chon{
		width: 100%;
		height: 2%;
		position: relative;
	}
	.vi_tri_tim_kiem{
		position: absolute;
		top: 10px;
		right: 10px;
	}

	.banner{
		margin-left: 5px;
		box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.5);
	}

	/*css icon*/
	.icon_project1{
		position: absolute;
		left: 10px
	}
	.anh_icon {
		position: fixed;
		top: 120px;
		left: 15px;
		z-index: 20;
		cursor: pointer;
	    width: 50px;
	    height: 50px;
	    text-align: center;
	    border-radius: 3px;
	    background-position: center center;
	    background-size: cover;
	    background-image: url('../images/icon_project1.jpg');
	    box-shadow: 0 0 0 2px rgba(255,255,255,.3);
	    transition: opacity .2s, border-radius .2s, box-shadow .2s;
	    transition-timing-function: ease-out;
	}
	.anh_icon:hover {
		opacity: 1;
		border-radius: 50%;
		box-shadow: 0 0 0 2px rgba(255,255,255,0);
	}

	.icon_come_back{
		margin: 5px;
	}
	
	/*css slide show*/
	.half {
		width: 50%;
	}

	.a-down {
		display: flex;
		justify-content: center;
		align-items: flex-end;
	}

	.a-center {
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.btn {
		display: block;
		min-width: 150px;
		text-align: center;
		background: rgba(0, 0, 0, 0.6);
		height: 48px;
		font-size: 0.7em;
		text-decoration: none;
		color: #fff;
		padding: 10px 0;
		border: 2px solid #dd4b39;
		border-radius: 3px;
	}
	.btn:hover {
		background: rgba(221, 75, 57, 0.4);
	}

		/* slide styles */
		.slide {
			color: #555;
			margin: 10px 3px;
			width: 100%;
			height: 60vh;
			overflow: hidden;
			position: relative;
			z-index: -1;
		}

		.slide-show {
			display: flex;
			width: 300vw;
			height: 100%;
			overflow: auto;
			position: absolute;
		}

		.slide-item {
			width: 100%;
			display: flex;
			flex-direction: row;
			align-items: center;
			padding: 0;
		}
		.slide-item img {
			width: 100vw;
			height: 100%;
		}

		.slide-text {
			width: 100%;
			display: flex;
			flex-direction: row;
			justify-content: center;
			align-items: center;
			position: absolute;
			top: 0;
			left: 0;
			height: 100%;
			background: rgba(0, 0, 0, 0.6);
			color: #f8f8f8;
			padding-bottom: 25px;
			font-size: 30px;
		}
		.slide-text div {
			height: 90%;
		}
		.slide-text div:nth-child(1) {
			flex-direction: column;
			padding: 0 5%;
			align-items: flex-start;
		}

		.slide-indicator {
			width: 100%;
			display: flex;
			flex-direction: row;
			justify-content: center;
			align-items: center;
			position: absolute;
			bottom: 0;
		}
		.slide-indicator .indicator-item {
			height: 3px;
			width: 70px;
			margin: 0 10px;
			background: rgba(170, 170, 170, 0.3);
			border: dodgerblue;
			list-style: none;
		}

		[rol="low-indicator"] {
			display: block;
			height: 100%;
			width: 1%;
			background: rgba(255, 255, 255, 0.7);
		}

		.blur-slide {
			filter: blur(3px);
		}

		/* smartphone */
		@media (max-width: 768px) {
		  .slide {
		  	color: #555;
		  	width: 100%;
		  	height: 80vh;
		  	overflow: hidden;
		  	position: relative;
		  	z-index: 1;
		  }

		  .slide-title {
		  	font-size: 20px;
		  }

		  .indicator-item {
		  	height: 2px;
		  	width: 50px;
		  	margin: 0 10px;
		  	background: rgba(170, 170, 170, 0.3);
		  	border: dodgerblue;
		  	list-style: none;
		  }
		}

	/*css form*/
	.span_gioi_tinh{
		font-style: italic;
	}
		/*css form hoa don*/
		.form_hoa_don{
			margin: 15px 5px;
			font-size: 18px;
			font-weight: bold;
		}
		.input_hoa_don{
			width: 175px;
			height: 50px;
			margin: 5px 5px;
		}
		.dieu_huong_hoa_don{
			height: 50px;
			margin: 5px 5px;
		}
		/*css form bootstrap*/
		#success_message{ display: none;}

	/*css table*/
	.mo_ta{
		font-style: italic;
		font-size: 14;
	}
	.tinh_trang{
		text-decoration: underline;
	}
	.gia_san_pham{
		font-weight: bold;
		font-size: 22;
	}

	/*css trang chu*/
	.quay_lai_trang_chu{
		margin-top: 10px;
	}
	
	.the_a_san_pham{
		transition: 0.6s;
		color: #ff662e;
		font-size: 24px;
	}

	.the_a_san_pham:hover{
		color: #ff9c85;
		text-shadow: 2px 2px 5px #5c5c33;
	}
	
	#css_san_pham{
		margin-top: 10px;
	}

	/*css doi mat khau*/
	#doi_mat_khau{
		margin-top: 10px;
	}
	.quay_lai_quan_ly_tai_khoan{
		margin-top: 10px;
	}

	/*css the a*/
	a{
		text-decoration: none;
	}

	/*css hoa don*/
	#xem_hoa_don{
		font-size: 24px;
		text-shadow: 1px 1px grey;
	}

	#xem_hoa_don a{
		transition: 0.2s;
	}

	#xem_hoa_don a:hover{
		color: #f28cd9;
		text-shadow: 2px 2px 5px #5c5c33;
	}

	/*css paging*/
	.paging{
		width: 100%;
		float: right;
	}
	.number_page a{
		transition: 0.3s ease;
		font-size: 24px;
		text-decoration: none;
		border-top: 4px solid #3fa46a;
		border-bottom: 4px solid #3fa46a;
		padding: 0 10px;
	}
	.number_page a:hover{
		color: #f28cd9;
		text-shadow: 2px 2px 5px #5c5c33;
	}

	/*css footer*/
	#footer{
		width: 100%;
		height: 7%;
		background:#ff80d9;
		margin-top: 5px;
		float: left;
		box-shadow: 5px -2px 10px rgba(0, 0, 0, 0.5);
		position: relative;
	}

	/*css footer left*/
	#footer_left{
		margin: 2px 3px;
		width: 70%;
	}

	/*css footer right*/
	#footer_right{
		width: 30%;
		float: left;
		position: absolute;
		right: -100px;
		top: 50px;
	}
	#icon_social {
		margin:0;
		padding:0;
	}
	#icon_social li {
		list-style:none;
		margin:0px 15px;
		padding:5px;
		display:inline-block;
		height:40px;
		width:40px;
	}
	svg {
		height:40px;
		width:40px;
		fill:#949899;
		-webkit-transition-property: all;
		-webkit-transition-duration: 0.2s;
		cursor:pointer;
	}
	svg:hover {
		fill:#fff;
	}
	svg:active {
		fill:#00aeef;
	}
</style>