<article class="slide">
  <div class="slide-show">
    <div class="slide-item"> <img class="slide-img" src="../images/slideshow_3.jpg" /></div>
    <div class="slide-item"> <img class="slide-img" src="../images/slideshow_2.jpg" /></div>
    <div class="slide-item"> <img class="slide-img" src="../images/slideshow_1.jpg" /></div>
  </div>
  <div class="slide-text">
    <div class="half a-center">
      <h2 class="alide-title"></h2>
    </div>
    <ul class="slide-indicator">
      <li class="indicator-item"><a rol="low-indicator"></a></li>
      <li class="indicator-item"><a rol="low-indicator"></a></li>
      <li class="indicator-item"><a rol="low-indicator"></a></li>
    </ul>
  </div>
</article>
<script src="../js_project1/jquery.min.js"></script>
<script src="../js_project1/js_slide_show.js"></script>