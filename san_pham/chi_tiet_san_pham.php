<?php
	session_start(); 
	if(isset($_GET["ma_san_pham"])){
		$ma_san_pham = $_GET["ma_san_pham"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chi tiet san pham</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>
	<?php
		include('../template_webbanhang/template_upper_part.php');
	?>

	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
				Chi tiet san pham
			</h1>
		</a>
	</div>

	<table>
		<?php
		
			$lenh = mysqli_query($ket_noi,"select san_pham.ma_san_pham, san_pham.ten_san_pham, san_pham.anh_san_pham, san_pham.gia_san_pham, san_pham.mo_ta, san_pham.tinh_trang, nha_san_xuat.ten_nha_san_xuat from san_pham inner join nha_san_xuat on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat where ma_san_pham = '$ma_san_pham'");
			include '../connecting/open.php';
			while($san_pham = mysqli_fetch_array($lenh)){
		?>
			<tr>
				<td rowspan="8">
					<img src="../images/<?php echo($san_pham["anh_san_pham"]); ?>" width="500px" height="650px" style="-moz-box-shadow: 1px 2px 4px rgba(0, 0, 0,0.5); -webkit-box-shadow: 1px 2px 4px rgba(0, 0, 0, .5); box-shadow: 1px 2px 4px rgba(0, 0, 0, .5); margin: 0 15px;">
				</td>
				<td>
					<h1 style="text-shadow: 0 0 5px white;">
						<?php 
							echo($san_pham["ten_san_pham"]);
						?>
					</h1>
				</td>
			</tr>
			<tr>
				<td>
					<div class="mo_ta">
						<?php 
							echo($san_pham["mo_ta"]);
						?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="tinh_trang">
					<?php
						if($san_pham["tinh_trang"]==0){
							echo "Con hang";
						}else{
							echo "Het hang";
						}
					?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="nha_san_xuat">
						<span>
							Nha san xuat:
							<?php
								echo($san_pham["ten_nha_san_xuat"]);
							?>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="gia_san_pham">
						<?php
							echo($san_pham["gia_san_pham"]);
						?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<a href="../gio_hang/them_vao_gio_hang.php?ma_san_pham=<?php echo $san_pham["ma_san_pham"] ?>" title="Them vao gio hang">
							<img src="../images/icon_add_cart.jpg" width="75px" height="65px" >
						</a>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<a href="../gio_hang/xem_gio_hang.php" title="Xem gio hang">
							<img src="../images/cart_icon.jpg" width="35px" height="35px" style="box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.5)">
						</a>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="icon_come_back">
						<a href="san_pham.php" title="Tiep tuc mua sam">
							<img src="../images/icon_come_back.jpg" width="35px" height="35px" >
						</a>
					</div>
				</td>
			</tr>
		<?php
			}
		?>
	</table>
	<?php 
		include '../template_webbanhang/template_footer.php';
	?>
</body>
</html>
<?php
	// Ngoac isset ma san pham
	}else{
		header("location:../trang_chu/trang_chu.php");
	}
?>