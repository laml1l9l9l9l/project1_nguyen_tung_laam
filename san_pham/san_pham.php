<?php
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan = $_SESSION["tai_khoan_khach_hang"];
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>San pham</title>
	<?php 
		include '../template_webbanhang/template_css.php';
	?>
</head>
<body>

<?php 
	include '../template_webbanhang/template_upper_part.php';
?>
	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
				San pham
			</h1>
		</a>
	</div>
	
<?php
	while($san_pham = mysqli_fetch_array($lenh_phan_trang)){
?>
<a href="../san_pham/chi_tiet_san_pham.php?ma_san_pham=<?php echo($san_pham["ma_san_pham"]); ?>" class="the_a_san_pham">
	<div style="width: 40%; float: left; margin-left: 100px;">

	<table>
		<tr>
			<td rowspan="3">
				<img src="../images/<?php echo($san_pham["anh_san_pham"]) ?>" width="300px" height="350px" style="-moz-box-shadow: 1px 2px 4px rgba(0, 0, 0,0.5); -webkit-box-shadow: 1px 2px 4px rgba(0, 0, 0, .5); box-shadow: 1px 2px 4px rgba(0, 0, 0, .5);">
			</td>
			<td>
				<h1>
				<?php 
					echo($san_pham["ten_san_pham"]);
				?>
				</h1>
			</td>
		</tr>
		<tr>
			<td>
			<?php 
				echo($san_pham["gia_san_pham"]);
			?>
			</td>
		</tr>
	</table>
	
	</div>
</a>
<?php
	// Ngoac cua while
	}
	
	// template phan trang
	include '../template_webbanhang/template_paging.php';
	// template duoi cung
	include '../template_webbanhang/template_footer.php';
?>
</body>
</html>