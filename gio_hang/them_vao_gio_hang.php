<?php
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan = $_SESSION["tai_khoan_khach_hang"];
		if(isset($_GET["ma_san_pham"])){
			$ma_san_pham = $_GET["ma_san_pham"];
			// Neu san pham da ton tai trong gio hang thi tang them 1
			if(isset($_SESSION["gio_hang"][$ma_san_pham])){
				$_SESSION["gio_hang"][$ma_san_pham]["so_luong"]++;
			}else{
				// them san pham chua co vao gio hang
				include("../connecting/open.php");
				$lenh = mysqli_query($ket_noi,"select * from san_pham where ma_san_pham = $ma_san_pham");
				include("../connecting/close.php");
				$san_pham = mysqli_fetch_array($lenh);
				// Luu vao session
				$_SESSION["gio_hang"][$ma_san_pham]["anh_san_pham"] = $san_pham["anh_san_pham"];
				$_SESSION["gio_hang"][$ma_san_pham]["ten_san_pham"] = $san_pham["ten_san_pham"];
				$_SESSION["gio_hang"][$ma_san_pham]["gia_san_pham"] = $san_pham["gia_san_pham"];
				$_SESSION["gio_hang"][$ma_san_pham]["so_luong"] = 1;
			}

			//Quay ve trang cu
			$lich_su = $_SERVER['HTTP_REFERER'];
			header("location:$lich_su");
			
		}
		else{
			header("location:../san_pham/san_pham.php");
		}
	}else{
		header("location:../login_khach_hang/login_khach_hang.php");
	}
?>