<?php
	session_start();
	if(isset($_SESSION["gio_hang"])){
		if(isset($_GET["thay_doi"]) && isset($_GET["ma_san_pham"])){
			$ma_san_pham = $_GET["ma_san_pham"];
			if($_GET['thay_doi']=="cong"){
				if($_SESSION["gio_hang"][$ma_san_pham]["so_luong"]==10){
					header("location:xem_gio_hang.php?loi_thay_doi_cong=1");
				}else{
					$_SESSION["gio_hang"][$ma_san_pham]["so_luong"]++;
					header("location:xem_gio_hang.php");
				}
			}else{
				if($_SESSION["gio_hang"][$ma_san_pham]["so_luong"]==1){
					header("location:xem_gio_hang.php?loi_thay_doi_tru=1");
				}else{
					$_SESSION["gio_hang"][$ma_san_pham]["so_luong"]--;
					header("location:xem_gio_hang.php");
				}
			}
		}else{
			header("location:xem_gio_hang.php");
		}
	}else{
		header("location:san_pham.php");
	}
?>