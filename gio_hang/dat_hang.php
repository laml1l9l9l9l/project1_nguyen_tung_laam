<?php 
	session_start();
	if(!empty($_SESSION["gio_hang"])){
		if(isset($_POST["ten_nguoi_nhan"]) && isset($_POST["sdt_nguoi_nhan"]) && isset($_POST["dia_chi_nguoi_nhan"]) && isset($_POST["tong_tien"])){
			$ten_nguoi_nhan = $_POST["ten_nguoi_nhan"];
			$sdt_nguoi_nhan = $_POST["sdt_nguoi_nhan"];
			$dia_chi_nguoi_nhan = $_POST["dia_chi_nguoi_nhan"];
			$tong_tien = $_POST["tong_tien"];
			$ngay_dat_hang = date("Y/m/d");
			$ma_khach_hang = $_SESSION["ma_khach_hang"];

			include '../connecting/open.php';

			// kiem tra neu co $ma_hoa_don thi tang len 1 - khong thi $ma_hoa_don = 1
			$lenh_tim_ma_hoa_don = mysqli_query($ket_noi,"select max(ma_hoa_don) from hoa_don");
			$lay_ma_hoa_don = mysqli_fetch_array($lenh_tim_ma_hoa_don);
			if(isset($lay_ma_hoa_don["max(ma_hoa_don)"])){
				$ma_hoa_don = $lay_ma_hoa_don["max(ma_hoa_don)"]+1;
			}else{
				$ma_hoa_don = 1;
			}
			
			// lenh them hoa don
			mysqli_query($ket_noi,"insert into hoa_don(ten_nguoi_nhan,so_dien_thoai_nguoi_nhan,dia_chi_nguoi_nhan,ngay_dat_hang,tinh_trang_giao_hang,thanh_tien,ma_khach_hang) values ('$ten_nguoi_nhan','$sdt_nguoi_nhan','$dia_chi_nguoi_nhan','$ngay_dat_hang',0,$tong_tien,$ma_khach_hang)");

			foreach ($_SESSION["gio_hang"] as $ma_san_pham => $san_pham) {
				$so_luong_san_pham = $san_pham["so_luong"];
				
				// lenh them hoa don chi tiet
				mysqli_query($ket_noi,"insert into hoa_don_chi_tiet(so_luong,ma_hoa_don,ma_san_pham) values ($so_luong_san_pham,'$ma_hoa_don','$ma_san_pham')");
			}

			include '../connecting/close.php';
			unset($_SESSION["gio_hang"]);
			$_SESSION["don_hang"][$ma_hoa_don] = $sdt_nguoi_nhan;
			header("location:xem_gio_hang.php?dat_hang_thanh_cong=1");

		}else{
			header("location:xem_gio_hang.php");
		}
	}else{
		header("location:../san_pham/san_pham.php");
	}
?>