<?php
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan = $_SESSION["tai_khoan_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Xem gio hang</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>
	<?php
		include('../template_webbanhang/template_upper_part.php');
	?>

	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">Gio hang</h1>
		</a>
	</div>

	<?php
		if(!empty($_SESSION["gio_hang"])){
			$tong = 0;
	?>
		
		<table width="100%" border="1" cellspacing="0">
			<tr>
				<td>
					<h2>San pham</h2>
				</td>
				<td>
					<h2>Ten san pham</h2>
				</td>
				<td>
					<h2>Gia san pham</h2>
				</td>
				<td>
					<h2>So luong</h2>
				</td>
				<td></td>
			</tr>
			
	<?php

		$lenh = mysqli_query($ket_noi,"select * from khach_hang where tai_khoan_khach_hang = '$tai_khoan'");
		$tai_khoan_khach_hang = mysqli_fetch_array($lenh);

		foreach($_SESSION["gio_hang"] as $ma_san_pham => $san_pham){
	?>
		<tr>
			<td>
				<img src="../images/<?php echo $san_pham["anh_san_pham"] ?>" width="150px" height="200px">
			</td>
			<td>
				<p style="font-size: 24px; font-weight: bold;">
				<?php
					echo $san_pham["ten_san_pham"];
				?>
				</p>
			</td>
			<td>
			<?php
				echo $san_pham["gia_san_pham"];
			?>
			</td>
			<td>

				<!-- tang/giam so luong san pham -->
				<a href="sua_so_luong_san_pham_gio_hang.php?thay_doi=tru&ma_san_pham=<?php echo $ma_san_pham ?>">-</a>
			<?php
				echo $san_pham["so_luong"];
			?>
				<a href="sua_so_luong_san_pham_gio_hang.php?thay_doi=cong&ma_san_pham=<?php echo $ma_san_pham ?>">+</a>

			<!-- bao loi tang/giam san pham -->
			<?php 
				if(isset($_GET["loi_thay_doi_tru"])){
					echo "So luong san pham da nho nhat";
				}
				if(isset($_GET["loi_thay_doi_cong"])){
					echo "So luong san pham dat gioi han";
				}
			?>

			</td>
			<td>

				<!-- Xoa san pham -->
				<a href="xoa_san_pham_gio_hang.php?ma_san_pham=<?php echo $ma_san_pham; ?>">
					Xoa san pham
				</a>

			</td>
		</tr>

	<?php
		$tong = $tong + $san_pham["gia_san_pham"]*$san_pham["so_luong"];
		}
		include '../connecting/close.php';
	?>
		</table>

		<!-- tong tien -->
		<div>
			<h2>
				Tong tien: <?php echo $tong ?> VND
			</h2>
		</div>

		<!-- form dat hang -->
		<div style="width:100%; margin: 20px 30px;">
			<form action="dat_hang.php" method="post">
				<table>
					<tr>
						<td>
							<h4>
								<label for="ten_nguoi_nhan">Ten nguoi nhan</label>
							</h4>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="ten_nguoi_nhan" id="ten_nguoi_nhan" value="<?php echo($tai_khoan_khach_hang["ten_khach_hang"]); ?>">
						</td>
					</tr>
					<tr>
						<td>
							<h4>
								<label for="sdt_nguoi_nhan">So dien thoai nguoi nhan</label>
							</h4>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="sdt_nguoi_nhan" id="sdt_nguoi_nhan" value="<?php echo($tai_khoan_khach_hang["so_dien_thoai"]); ?>">
						</td>
					</tr>
					<tr>
						<td>
							<h4>
								<label for="dia_chi_nguoi_nhan">Dia chi nguoi nhan</label>
							</h4>
						</td>
					</tr>
					<tr>
						<td>
							<textarea name="dia_chi_nguoi_nhan" id="dia_chi_nguoi_nhan"><?php echo($tai_khoan_khach_hang["dia_chi"]); ?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<button style="width:100px; height: 30px" type="submit">
								Dat hang
							</button>
						</td>
					</tr>
				</table>
				<input type="hidden" name="tong_tien" id="tong_tien" value="<?php echo($tong); ?>">
			</form>
		</div>

			<!-- Huy gio hang -->
			<a href="xoa_gio_hang.php">
				<button style="width:200px; height: 30px; margin-top: 10px;" type="submit">
					Huy gio hang
				</button>
			</a>

		</div>
	<?php

		// ngoac neu ton tai gio hang
		}else{
	?>

		<!-- Khi chua ton tai gioa hang -->
		<h2>
	<?php
		if(isset($_GET["dat_hang_thanh_cong"])){
			echo "Dat hang thanh cong. Nhan nut quay lai trang chu de tiep tuc mua sam";
		}else{
			echo "Chua co san pham trong gio hang. Tiep tuc mua sam";
		}
	?>
		</h2>
		<div class="icon_come_back">
			<a href="../trang_chu/trang_chu.php" title="Quay lai trang chu">
				<img src="../images/icon_come_back.jpg" width="35px" height="35px">
			</a>
		</div>

	<?php
		}

		include('../template_webbanhang/template_footer.php');
	?>
</body>
</html>
<?php
	//Ngoac neu ton tai session tai khoan 
	}else{
		header("location:../login_khach_hang/login_khach_hang.php");
	}
?>