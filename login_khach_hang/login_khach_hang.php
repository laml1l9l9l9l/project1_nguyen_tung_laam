<?php 
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		header("location:../trang_chu/trang_chu.php");
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css">
	<style type="text/css">
		
	</style>

</head>
<body>
	<?php
		include('../template_webbanhang/template_upper_part.php');
	?>

	<!-- banner -->
	<div class="banner">
		<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">Dang nhap</h1>
	</div>

<!-- Form login -->
<div class="container" style="margin-top: 50px;">
	<form class="well form-horizontal" id="contact_form" method="post" action="xu_ly_login.php">
		<fieldset>
			<!-- Tai khoan -->

			<div class="form-group">
				<label class="col-md-4 control-label" for="tai_khoan">Tai khoan</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input  name="tai_khoan" id="tai_khoan" placeholder="Nhap tai khoan" class="form-control"  type="text">
					</div>
				</div>
			</div>

			<!-- Mat khau -->

			<div class="form-group">
				<label class="col-md-4 control-label" for="mat_khau">Mat khau</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input name="mat_khau" id="mat_khau" placeholder="Nhap mat khau" class="form-control"  type="password">
					</div>
				</div>
			</div>
		<!-- <table cellpadding="15px">
			<tr>
				<td>
					<label for="tai_khoan">Tai Khoan</label>
					<br>
					<input type="text" name="tai_khoan" id="tai_khoan">
					<span id="loi_tai_khoan"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="mat_khau">Mat Khau</label>
					<br>
					<input type="password" name="mat_khau" id="mat_khau">
					<span id="loi_mat_khau"></span>
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" onclick="return check_form_login()" id="button_login">
						Dang nhap
					</button>
				</td>
			</tr>
		</table> -->

		<!-- Nut dang ky -->
			<div class="form-group">
				<label class="col-md-4 control-label"></label>
				<div class="col-md-4">
					<button type="submit" class="btn btn-warning" >Dang nhap <span class="glyphicon glyphicon-send"></span></button>
				</div>
			</div>

		</fieldset>
	</form>

	<!-- Bao loi -->
		<?php 
			if(isset($_GET["loi"])){
				echo "<h3>Ban nhap chua dung tai khoan hoac mat khau</h3>";
			}
		?>

</div>

	<script type="text/javascript" src="../js_project1/jquery.min.js"></script>
	<script type="text/javascript" src="../js_project1/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js_project1/bootstrapvalidator.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		// regex the input
			$('#contact_form').bootstrapValidator({
				feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					tai_khoan: {
						validators: {
							stringLength: {
								min: 4,
								message: 'Phai nhap nhieu hon 4 ky tu'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^[a-z0-9\-\_]+$/,
								message: 'Khong duoc nhap ky tu dac biet va khong viet hoa'
							}
						}
					},
					mat_khau: {
						validators: {
							stringLength: {
								min: 6,
								message: 'Phai nhap nhieu hon 6 ky tu'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^[a-z0-9\-\_\@\!\#\$\%\^\&\*]+$/i,
								message: 'Mat khau khong chua ngoa hoac khoang trong'
							}
						}
					}
				}
			});
		});
	</script>

	<!-- <script type="text/javascript">
		function check_form_login(){
			var dem = 0;

			var tai_khoan = document.getElementById("tai_khoan").value;
			var regex_tai_khoan = /^[a-z0-9\-\_]+$/;
			var check_tai_khoan = regex_tai_khoan.test(tai_khoan);
			if(tai_khoan.length == 0){
				document.getElementById("loi_tai_khoan").innerHTML = 'Khong duoc de trong';
				dem = 1;
			}else{
				if(check_tai_khoan == false){
					document.getElementById("loi_tai_khoan").innerHTML = 'Nhap sai dinh dang';
					dem = 1;
				}else{
					document.getElementById("loi_tai_khoan").innerHTML = '';
				}
			}

			var mat_khau = document.getElementById("mat_khau").value;
			var regex_mat_khau = /^[a-z0-9\-\_\@\!\#\$\%\^\&\*\.\,]+$/i;
			var check_mat_khau = regex_tai_khoan.test(mat_khau);
			if(mat_khau.length == 0){
				document.getElementById("loi_mat_khau").innerHTML = 'Khong duoc de trong';
				dem = 1;
			}else{
				if(check_mat_khau == false){
					document.getElementById("loi_mat_khau").innerHTML = 'Nhap sai dinh dang';
					dem = 1;
				}else{
					document.getElementById("loi_mat_khau").innerHTML = '';
				}
			}

			// return
			if(dem == 1){
				return false;
			}else{
				document.getElementById("button_login").submit();
			}
		}
	</script> -->
</body>
</html>