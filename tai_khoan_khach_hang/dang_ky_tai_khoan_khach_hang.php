<?php 
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		header("location:../trang_chu/trang_chu.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dang ky tai khoan khach hang</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css">
	<style type="text/css">
		#success_message{ display: none;}
	</style>

</head>
<body>
	<?php
		include('../template_webbanhang/template_upper_part.php');
		include '../connecting/close.php';
	?>
	
	<!-- banner -->
	<div class="banner">
		<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
			Dang ky tai khoan
		</h1>
	</div>

<div class="container">
	<form class="well form-horizontal" id="contact_form" method="post" action="xu_ly_dang_ky.php">
		<fieldset>

			<!-- Form dang ky -->
			<legend style="text-align: center; font-size: 24; font-weight: bold;">Nhap dung thong tin</legend>

			<!-- Tai khoan -->

			<div class="form-group">
				<label class="col-md-4 control-label" for="tai_khoan">Tai khoan</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input  name="tai_khoan" id="tai_khoan" placeholder="Nhap tai khoan" class="form-control"  type="text">
					</div>
				</div>
			</div>

			<!-- Mat khau -->

			<div class="form-group">
				<label class="col-md-4 control-label" for="mat_khau">Mat khau</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input name="mat_khau" id="mat_khau" placeholder="Nhap mat khau" class="form-control"  type="password">
					</div>
				</div>
			</div>

			<!-- Ho va ten -->

			<div class="form-group">
				<label class="col-md-4 control-label" for="ho_ten">Ho va Ten</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input  name="ho_ten" id="ho_ten" placeholder="Nhap du ho va ten" class="form-control"  type="text">
					</div>
				</div>
			</div>

			<!-- Ngay sinh -->

			<div class="form-group"> 
				<label class="col-md-4 control-label">Ngay sinh</label>
				<div class="col-md-4 selectContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						<select name="nam_sinh" id="nam_sinh" class="form-control selectpicker" >
							<option value="0" >Nam</option>
						</select>
						<select name="thang_sinh" id="thang_sinh" class="form-control selectpicker" >
							<option value="0" >Thang</option>
						</select>
						<select name="ngay_sinh" id="ngay_sinh" class="form-control selectpicker" >
							<option value="0" >Ngay</option>
						</select>
					</div>
				</div>
			</div>

			<!-- Gioi tinh -->
			<div class="form-group">
				<label class="col-md-4 control-label">Gioi tinh</label>
				<div class="col-md-4">
					<div class="radio">
						<label for="gioi_tinh">
							<input type="radio" name="gioi_tinh" id="gioi_tinh" value="0" /> Nam
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="gioi_tinh" value="1" /> Nu
						</label>
					</div>
				</div>
			</div>

			<!-- Email -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="email">E-Mail</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
						<input name="email" id="email" placeholder="Nhap dia chi E-Mail" class="form-control"  type="text">
					</div>
				</div>
			</div>


			<!-- So dien thoai -->

			<div class="form-group">
				<label class="col-md-4 control-label" for="so_dien_thoai">So dien thoai</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
						<input name="so_dien_thoai" id="so_dien_thoai" placeholder="(+84)231155235/ 0354321864" class="form-control" type="text">
					</div>
				</div>
			</div>

			<!-- Dia chi -->

			<div class="form-group">
				<label class="col-md-4 control-label" for="dia_chi">Dia chi</label>
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
						<textarea class="form-control" name="dia_chi" id="dia_chi" placeholder="Dia chi cua ban"></textarea>
					</div>
				</div>
			</div>

			<!-- Nut dang ky -->
			<div class="form-group">
				<label class="col-md-4 control-label"></label>
				<div class="col-md-4">
					<button type="submit" class="btn btn-warning" >Dang ky <span class="glyphicon glyphicon-send"></span></button>
				</div>
			</div>

		</fieldset>
	</form>

	<div class="icon_come_back">
		<a href="../trang_chu/trang_chu.php" title="Quay lai trang chu">
			<img src="../images/icon_come_back.jpg" width="35px" height="35px">
		</a>
	</div>

</div>

	<script type="text/javascript" src="../js_project1/jquery.min.js"></script>
	<script type="text/javascript" src="../js_project1/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js_project1/bootstrapvalidator.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// jquery ngay/thang/nam
			for(var i=1; i<=31; i++){
				$("#ngay_sinh").append(`<option value='${i}'>Ngày ${i}</option>`);
			}
			for(var i=1; i<=12; i++){
				$("#thang_sinh").append(`<option value='${i}'>Tháng ${i}</option>`);
			}
			var hien_tai = new Date();
			var nam = hien_tai.getFullYear();
			for(var j=1900; j<=nam; j++){
				$("#nam_sinh").append(`<option value='${j}'>Năm ${j}</option>`);
			}
			$("#thang_sinh, #nam_sinh").change(function(){
				var thang = $('#thang_sinh').val();
				thang = parseInt(thang);
				var ngay = 31;
				switch(thang)
				{
					case 4:
					case 6:
					case 9:
					case 11:
						ngay=30;
						break;
					case 2:
						var nam = $("#nam_sinh").val();
						if(nam%4==0)
						{
							ngay = 29;
						}else
						{
							ngay = 28;
						}
						break;
				}
				$("#ngay_sinh").html('');
				for(var i=1; i<=ngay; i++)
				{
					$("#ngay_sinh").append(`<option value='${i}'>Ngày ${i}</option>`);
				}
			});

			// regex the input
			$('#contact_form').bootstrapValidator({
				feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					tai_khoan: {
						validators: {
							stringLength: {
								min: 4,
								message: 'Phai nhap nhieu hon 4 ky tu'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^[a-z0-9\-\_]+$/,
								message: 'Khong duoc nhap ky tu dac biet va khong viet hoa'
							}
						}
					},
					mat_khau: {
						validators: {
							stringLength: {
								min: 6,
								message: 'Phai nhap nhieu hon 6 ky tu'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^[a-z0-9\-\_\@\!\#\$\%\^\&\*]+$/i,
								message: 'Mat khau khong chua ngoa hoac khoang trong'
							}
						}
					},
					ho_ten: {
						validators: {
							stringLength: {
								min: 2,
								message: 'Phai nhap nhieu hon 2 ky tu'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^([A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪ][a-zàáâãèéêếìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹ]{1,6}\s?){2,6}$/,
								message: 'Chu bat dau phai la chu viet hoa va co du ho va ten'
							}
						}
					},
					email: {
						validators: {
							stringLength: {
								min: 6,
								message: 'Phai nhap nhieu hon 6 ky tu'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^[a-zA-Z0-9\_\-]+\@[a-zA-Z0-9]+\.([a-zA-Z]{2,3}\.)?[a-zA-Z]{2,3}$/,
								message: 'Nhap sai dinh dang abc@gmail.com'
							}
						}
					},
					so_dien_thoai: {
						validators: {
							stringLength: {
								min: 10,
								message: 'Phai nhap nhieu hon 9 so'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^[\+]?[0-9]{10,11}$/,
								message: 'Nhap khong qua 12 ky tu'
							}
						}
					},
					dia_chi: {
						validators: {
							stringLength: {
								min: 2,
								message: 'Phai nhap nhieu hon 2 ky tu'
							},
							notEmpty: {
								message: 'Khong duoc de trong'
							},
							regexp: {
								regexp: /^[a-z0-9]+[\,\. a-z0-9]*[a-z0-9]$/i,
								message: 'Dia khong chua ky tu dac biet va khong duoc thua khoang trong'
							}
						}
					}
				}
			})
		});


	</script>

</body>
</html>