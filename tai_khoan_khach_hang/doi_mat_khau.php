<?php
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan_khach_hang = $_SESSION["tai_khoan_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Doi mat khau</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>
	<?php
		include('../template_webbanhang/template_upper_part.php');
		include '../connecting/close.php';
	?>

	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">Doi mat khau</h1>
		</a>
	</div>

	<form action="xu_ly_doi_mat_khau.php" method="post">
		<table cellpadding="10px">
			<tr>
				<td>
					<label for="mat_khau_cu">Mat khau cu</label>
					<br>
					<input type="password" name="mat_khau_cu" id="mat_khau_cu">
				</td>
			</tr>
			<tr>
				<td>
					<label for="mat_khau_moi">Mat khau moi</label>
				<br>
					<input type="password" name="mat_khau_moi" id="mat_khau_moi">
				</td>
			</tr>
			<tr>
				<td>
					<label for="nhap_lai_mat_khau_moi">Nhap lai mat khau moi</label>
				<br>
					<input type="password" name="nhap_lai_mat_khau_moi" id="nhap_lai_mat_khau_moi">
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit">
						Thay doi mat khau
					</button>
					<span>
						<?php
							if(isset($_GET["loi"]) || isset($_GET["loi_de_trong"])){
								if(isset($_GET["loi"])){
									if($_GET["loi"] == 1){
										echo "Ban nhap mat khau moi khong khop";
									}else{
										echo "Ban nhap sai mat khau cu";
									}
								}else{
									echo "Ban khong duoc de trong";
								}
							}
						?>
					</span>
				</td>
			</tr>
		</table>
	</form>
	<div class="icon_come_back">
		<a href="quan_ly_tai_khoan.php" title="Quay lai trang quan ly tai khoan">
			<img src="../images/icon_come_back.jpg" width="35px" height="35px">
		</a>
	</div>
</body>
</html>
<?php
	// Neu ton tai tai khoan
	}else{
		header("location:../login_khach_hang/login_khach_hang.php");
	}
?>