<?php 
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan_khach_hang = $_SESSION["tai_khoan_khach_hang"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Quan ly tai khoan</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>
	<?php
		include('../template_webbanhang/template_upper_part.php');
		// lay thong tin khach hang ve
		$lenh = mysqli_query($ket_noi,"select * from khach_hang where tai_khoan_khach_hang='$tai_khoan_khach_hang'");
		$khach_hang = mysqli_fetch_array($lenh);
		include('../connecting/close.php');
	?>

	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
				Quan ly tai khoan
			</h1>
		</a>
	</div>

	<div style="width: 50%; float: left">
		<!-- Form quan ly tai khoan -->
		<form method="post" action="cap_nhat_thong_tin.php" method="post">
			<table cellpadding="10px">
				<tr>
					<td>
						Tai Khoan
						<br>
						<input type="text" name="tai_khoan" id="tai_khoan" value="<?php echo($khach_hang["tai_khoan_khach_hang"]); ?>" readonly>
					</td>
				</tr>
				<tr>
					<td>
						Ho va ten
						<br>
						<input type="text" name="ho_ten" id="ho_ten" value="<?php echo($khach_hang["ten_khach_hang"]); ?>">
						<span id="loi_ho_ten_form_quan_ly"></span>
					</td>
				</tr>
				<tr>
					<td>
						Ngay sinh
						<br>
						<input type="text" name="ngay_sinh" id="ngay_sinh" size="15" value="<?php $ngay_sinh = $khach_hang["ngay_sinh"]; echo date("j/m/Y", strtotime($ngay_sinh)); ?>" readonly>
					</td>
				</tr>
				<tr>
					<td>
						Gioi tinh
						<br>
						<input type="radio" name="gioi_tinh" class="gioi_tinh" <?php if($khach_hang["gioi_tinh"]==0){ ?> checked="checked" <?php } ?> value="0">
						<span class="span_gioi_tinh">Nam</span>
						<input type="radio" name="gioi_tinh" class="gioi_tinh" <?php if($khach_hang["gioi_tinh"]==1){ ?> checked="checked" <?php } ?> value="1">
						<span class="span_gioi_tinh">Nu</span>
					</td>
				</tr>
				<tr>
					<td>
						Email
						<br>
						<input type="text" name="email" id="email" value="<?php echo($khach_hang["email_khach_hang"]); ?>">
						<span id="loi_email_form_quan_ly"></span>
					</td>
				</tr>
				<tr>
					<td>
						So dien thoai
						<br>
						<input type="text" name="so_dien_thoai" id="so_dien_thoai" size="15" value="<?php echo($khach_hang["so_dien_thoai"]); ?>">
						<span id="loi_sdt_form_quan_ly"></span>
					</td>
				</tr>
				<tr>
					<td>
						Dia chi
						<br>
						<textarea name="dia_chi" id="dia_chi" cols="50" rows="2"><?php echo($khach_hang["dia_chi"]); ?></textarea>
						<span id="loi_dia_chi_form_quan_ly"></span>
					</td>
				</tr>
				<tr>
					<td>
						<button type="submit" id="cap_nhat_tai_khoan" onclick="return update_id()">
							Cap nhat
						</button>
						<span>
							<?php
								if(isset($_GET["cap_nhat_thanh_cong"])){
									echo "Cap nhat thong tin thanh cong";
								}
							?>
						</span>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div style="width: 45%; margin:0px 10px; float: left;">
		<div id="xem_hoa_don">
			<a href="../hoa_don_khach_hang/hoa_don.php">
				Xem hoa don
			</a>
		</div>

		<!-- doi mat khau -->
		<a href="doi_mat_khau.php">
			<button type="submit" id="doi_mat_khau">
				Doi mat khau
			</button>
		</a>
		<span>
			<?php 
				if(isset($_GET["thanh_cong"])){
					echo "Doi mat khau thanh cong";
				}
			 ?>
		</span>

		<!-- icon come back -->
		<div class="icon_come_back">
			<a href="../trang_chu/trang_chu.php" title="Quay lai trang chu">
				<img src="../images/icon_come_back.jpg" width="35px" height="35px">
			</a>
		</div>
	</div>

	<script type="text/javascript">
		function update_id(){
			var dem_loi_cap_nhat_id = 0;

			var ho_ten = document.getElementById("ho_ten").value;
			var regex_ho_ten = /^[a-z0-9\_\-\s]+$/i;
			var kiem_tra_ho_ten = regex_ho_ten.test(ho_ten);
			if(ho_ten.length == 0){
				document.getElementById("loi_ho_ten_form_quan_ly").innerHTML = 'Khong duoc de trong';
				dem_loi_cap_nhat_id = 1;
			}else {
				if(kiem_tra_ho_ten == false){
					document.getElementById("loi_ho_ten_form_quan_ly").innerHTML = 'Nhap sai dinh dang';
					dem_loi_cap_nhat_id = 1;
				}else{
					document.getElementById("loi_ho_ten_form_quan_ly").innerHTML = '';
				}
			}

			var email = document.getElementById("email").value;
			var regex_email = /^[a-zA-Z0-9\_\-]+\@[a-zA-Z0-9]+\.([a-zA-Z]{2,3}\.)?[a-zA-Z]{2,3}$/;
			var kiem_tra_email = regex_email.test(email);
			if(email.length == 0){
				document.getElementById("loi_email_form_quan_ly").innerHTML = 'Khong duoc de trong';
				dem_loi_cap_nhat_id = 1;
			}else {
				if(kiem_tra_email == false){
					document.getElementById("loi_email_form_quan_ly").innerHTML = 'Nhap sai dinh dang';
					dem_loi_cap_nhat_id = 1;
				}else{
					document.getElementById("loi_email_form_quan_ly").innerHTML = '';
				}
			}

			var so_dien_thoai = document.getElementById("so_dien_thoai").value;
			var regex_so_dien_thoai = /^[\+]?[0-9]{10,12}$/;
			var kiem_tra_so_dien_thoai = regex_so_dien_thoai.test(so_dien_thoai);
			if(so_dien_thoai.length == 0){
				document.getElementById("loi_sdt_form_quan_ly").innerHTML = 'Khong duoc de trong';
				dem_loi_cap_nhat_id = 1;
			}else {
				if(kiem_tra_so_dien_thoai == false){
					document.getElementById("loi_sdt_form_quan_ly").innerHTML = 'Nhap sai dinh dang';
					dem_loi_cap_nhat_id = 1;
				}else{
					document.getElementById("loi_sdt_form_quan_ly").innerHTML = '';
				}
			}

			var dia_chi = document.getElementById("dia_chi").value;
			var regex_dia_chi = /^[a-z0-9]+[\,\. a-z0-9]*[a-z0-9]+$/i;
			var kiem_tra_dia_chi = regex_dia_chi.test(dia_chi);
			if(dia_chi.length == 0){
				document.getElementById("loi_dia_chi_form_quan_ly").innerHTML = 'Khong duoc de trong';
				dem_loi_cap_nhat_id = 1;
			}else {
				if(kiem_tra_dia_chi == false){
					document.getElementById("loi_dia_chi_form_quan_ly").innerHTML = 'Nhap sai dinh dang';
					dem_loi_cap_nhat_id = 1;
				}else{
					document.getElementById("loi_dia_chi_form_quan_ly").innerHTML = '';
				}
			}

			// Hien thi loi
			if(dem_loi_cap_nhat_id == 1){
				return false;
			}else {
				document.getElementById("cap_nhat_tai_khoan").submit();
			}
		}
	</script>
	
</body>
</html>
<?php
	// Ngoac cua if isset
	}else{
			echo "Ban chua dang nhap";
		?>
			<a href="../login_khach_hang/login_khach_hang.php">Dang nhap</a>
		<?php
		}
?>