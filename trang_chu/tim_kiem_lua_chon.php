<?php
	session_start();
	if(isset($_GET["tim_kiem_lua_chon"]) || isset($_SESSION["tim_kiem_lua_chon"])){
		$_SESSION["tim_kiem_lua_chon"] = $_GET["tim_kiem_lua_chon"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Trang chu</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>
	
	<div style="width: 100%; position: relative;">
<?php
	include '../template_webbanhang/template_upper_part.php';
?>
	
	<!-- banner -->
	<div class="banner">
		<a href="">
			<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
				San pham
			</h1>
		</a>
	</div>

<?php
	include '../template_webbanhang/template_slide_show.php';
?>
	<div class="tim_kiem_theo_lua_chon">
		<div class="vi_tri_tim_kiem">
			<form id="tim_kiem_lua_chon" action="tim_kiem_lua_chon.php">
				<select name="tim_kiem_lua_chon" onchange="document.getElementById('tim_kiem_lua_chon').submit();">
					<option value="0">Moi nhat</option>
					<option value="1" <?php if($_SESSION["tim_kiem_lua_chon"] == 1){ ?> selected <?php } ?> >Gia: Thap -> Cao</option>
					<option value="2" <?php if($_SESSION["tim_kiem_lua_chon"] == 2){ ?> selected <?php } ?> >Gia: Cao -> Thap</option>
				</select>
			</form>
		</div>
	</div>
<?php
	if($_SESSION["tim_kiem_lua_chon"] == 0){
		include '../connecting/open.php';
		$san_pham_trang_chu = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem%' order by ma_san_pham desc  limit $limit offset $offset");
	}
	else if($_SESSION["tim_kiem_lua_chon"] == 1){
		include '../connecting/open.php';
		$san_pham_trang_chu = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem%' order by gia_san_pham asc limit $limit offset $offset");
		
	}else{
		include '../connecting/open.php';
		$san_pham_trang_chu = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem%' order by gia_san_pham desc limit $limit offset $offset");
	}

	while($san_pham = mysqli_fetch_array($san_pham_trang_chu)){
?>
	<a href="../san_pham/chi_tiet_san_pham.php?ma_san_pham=<?php echo($san_pham["ma_san_pham"]); ?>" class="the_a_san_pham">
		<div id="css_san_pham" style="float: left; width: 50%; text-align: center;">
			<div style="width:100%;">
				<img src="../images/<?php echo($san_pham["anh_san_pham"]) ?>" width="300px" height="350px">
			</div>
			<div id="css_thong_tin_san_pham">
				<div style="font-size: 20; font-weight: bold;">
					<?php 
						echo($san_pham["ten_san_pham"]);
					?>
				</div>
				<div>
					<?php 
						echo($san_pham["gia_san_pham"]);
					?>
				</div>
			</div>
			<div id="css_xem_chi_tiet_san_pham" style="padding-bottom: 5px;">
				<div>
					<button type="submit">
							Xem chi tiet
					</button>
				</div>
			</div>
		</div>
	</a>
<?php
	// Ngoac cua while
	}
?>
	</div>
	
	<div class="paging" align="center">
<?php
	$lenh_dem_tim_kiem = mysqli_query($ket_noi,"select count(*) as dem_tim_kiem from san_pham where ten_san_pham like '%$tim_kiem%'");
	$lay_san_pham = mysqli_fetch_array($lenh_dem_tim_kiem);
	$tong_san_pham = $lay_san_pham["dem_tim_kiem"];
	$san_pham_1_trang = ceil($tong_san_pham/$limit);

	for ($i=1; $i<=$san_pham_1_trang ; $i++) { 
?>
	<span class="number_page">
		<a href="?tim_kiem_lua_chon=<?php echo $_SESSION["tim_kiem_lua_chon"]; ?>&page=<?php echo $i; ?>&tim_kiem=<?php echo $tim_kiem; ?>">
			<?php echo $i; ?>
		</a>
	</span>
<?php
	}

	include '../connecting/close.php';
?>
	</div>
<?php
	include '../template_webbanhang/template_footer.php';
?>

</body>
</html>
<?php
	}else{
		header("location:trang_chu.php");
	}
?>