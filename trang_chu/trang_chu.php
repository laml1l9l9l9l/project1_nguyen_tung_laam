<?php
	session_start();
	if(isset($_SESSION["tai_khoan_khach_hang"])){
		$tai_khoan = $_SESSION["tai_khoan_khach_hang"];
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Trang chu</title>
	<?php 
		include('../template_webbanhang/template_css.php');
	?>
</head>
<body>

<div class="tong">
	<div style="width: 100%; position: relative;">
	<?php 
		include '../template_webbanhang/template_upper_part.php';
	?>
		
		<!-- banner -->
		<div class="banner">
			<a href="">
				<h1 style="font-style: italic; color: #a370ff; background-image: url('../images/background_banner.jpg');" align="center">
					San pham moi nhat
				</h1>
			</a>
		</div>
		
	<?php
		include '../template_webbanhang/template_slide_show.php';
		$san_pham_trang_chu = mysqli_query($ket_noi,"select * from san_pham where ten_san_pham like '%$tim_kiem%' order by ma_san_pham desc  limit $limit offset $offset");
	?>

		<div class="tim_kiem_theo_lua_chon">
			<div class="vi_tri_tim_kiem">
				<form id="tim_kiem_lua_chon" action="tim_kiem_lua_chon.php">
					<select name="tim_kiem_lua_chon" onchange="document.getElementById('tim_kiem_lua_chon').submit();">
						<option value="0">Moi nhat</option>
						<option value="1">Gia: Thap -> Cao</option>
						<option value="2">Gia: Cao -> Thap</option>
					</select>
				</form>
			</div>
		</div>

	<?php
		while($san_pham = mysqli_fetch_array($san_pham_trang_chu)){
	?>
		<a href="../san_pham/chi_tiet_san_pham.php?ma_san_pham=<?php echo($san_pham["ma_san_pham"]); ?>" class="the_a_san_pham">
			<div id="css_san_pham" style="float: left; width: 50%; text-align: center;">
				<div style="width:100%;" id="anh_san_pham">
					<img src="../images/<?php echo($san_pham["anh_san_pham"]) ?>" width="300px" height="350px" style="-moz-box-shadow: 1px 2px 4px rgba(0, 0, 0,0.5); -webkit-box-shadow: 1px 2px 4px rgba(0, 0, 0, .5); box-shadow: 1px 2px 4px rgba(0, 0, 0, .5);">
				</div>
				<div id="css_thong_tin_san_pham">
					<div style="font-size: 24; font-weight: bold;">
						<?php 
							echo($san_pham["ten_san_pham"]);
						?>
					</div>
					<div>
						<?php 
							echo($san_pham["gia_san_pham"]);
						?>
					</div>
				</div>
			</div>
		</a>
	<?php
		// Ngoac cua while
		}
	?>
	</div>

	<?php
		include '../template_webbanhang/template_paging.php';
		include '../template_webbanhang/template_footer.php';
	?>
</div>

</body>
</html>