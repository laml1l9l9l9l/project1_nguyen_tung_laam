-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2018 at 05:43 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `ma_admin` int(11) NOT NULL AUTO_INCREMENT,
  `tai_khoan_admin` varchar(200) DEFAULT NULL,
  `mat_khau` varchar(200) NOT NULL,
  `ten_admin` varchar(200) NOT NULL,
  `email_admin` varchar(200) DEFAULT NULL,
  `so_dien_thoai` char(13) DEFAULT NULL,
  `phan_quyen` tinyint(4) NOT NULL,
  PRIMARY KEY (`ma_admin`),
  UNIQUE KEY `tai_khoan_admin` (`tai_khoan_admin`),
  UNIQUE KEY `email_admin` (`email_admin`),
  UNIQUE KEY `so_dien_thoai` (`so_dien_thoai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `danh_muc`
--

CREATE TABLE IF NOT EXISTS `danh_muc` (
  `ma_danh_muc` int(11) NOT NULL AUTO_INCREMENT,
  `ten_danh_muc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ma_danh_muc`),
  UNIQUE KEY `ten_danh_muc` (`ten_danh_muc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hoa_don`
--

CREATE TABLE IF NOT EXISTS `hoa_don` (
  `ma_hoa_don` int(11) NOT NULL AUTO_INCREMENT,
  `ten_nguoi_nhan` varchar(200) NOT NULL,
  `so_dien_thoai_nguoi_nhan` char(13) NOT NULL,
  `dia_chi_nguoi_nhan` text NOT NULL,
  `ngay_dat_hang` date NOT NULL,
  `tinh_trang_giao_hang` tinyint(4) NOT NULL,
  `thanh_tien` float NOT NULL,
  `ma_khach_hang` int(11) NOT NULL,
  PRIMARY KEY (`ma_hoa_don`),
  KEY `ma_khach_hang` (`ma_khach_hang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hoa_don_chi_tiet`
--

CREATE TABLE IF NOT EXISTS `hoa_don_chi_tiet` (
  `so_luong` int(11) NOT NULL,
  `ma_hoa_don` int(11) NOT NULL DEFAULT '0',
  `ma_san_pham` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ma_hoa_don`,`ma_san_pham`),
  KEY `ma_san_pham` (`ma_san_pham`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `khach_hang`
--

CREATE TABLE IF NOT EXISTS `khach_hang` (
  `ma_khach_hang` int(11) NOT NULL AUTO_INCREMENT,
  `tai_khoan_khach_hang` varchar(200) DEFAULT NULL,
  `mat_khau` varchar(200) NOT NULL,
  `ten_khach_hang` varchar(200) NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` tinyint(4) NOT NULL,
  `email_khach_hang` varchar(200) DEFAULT NULL,
  `so_dien_thoai` char(13) DEFAULT NULL,
  `dia_chi` text NOT NULL,
  PRIMARY KEY (`ma_khach_hang`),
  UNIQUE KEY `tai_khoan_khach_hang` (`tai_khoan_khach_hang`),
  UNIQUE KEY `email_khach_hang` (`email_khach_hang`),
  UNIQUE KEY `so_dien_thoai` (`so_dien_thoai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `nha_san_xuat`
--

CREATE TABLE IF NOT EXISTS `nha_san_xuat` (
  `ma_nha_san_xuat` int(11) NOT NULL AUTO_INCREMENT,
  `ten_nha_san_xuat` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ma_nha_san_xuat`),
  UNIQUE KEY `ten_nha_san_xuat` (`ten_nha_san_xuat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `san_pham`
--

CREATE TABLE IF NOT EXISTS `san_pham` (
  `ma_san_pham` int(11) NOT NULL AUTO_INCREMENT,
  `ten_san_pham` varchar(200) DEFAULT NULL,
  `anh_san_pham` varchar(200) DEFAULT NULL,
  `gia_san_pham` float NOT NULL,
  `mo_ta` text NOT NULL,
  `tinh_trang` tinyint(4) NOT NULL,
  `ma_nha_san_xuat` int(11) NOT NULL,
  `ma_danh_muc` int(11) NOT NULL,
  PRIMARY KEY (`ma_san_pham`),
  UNIQUE KEY `ten_san_pham` (`ten_san_pham`),
  UNIQUE KEY `anh_san_pham` (`anh_san_pham`),
  KEY `ma_nha_san_xuat` (`ma_nha_san_xuat`),
  KEY `ma_danh_muc` (`ma_danh_muc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD CONSTRAINT `hoa_don_ibfk_1` FOREIGN KEY (`ma_khach_hang`) REFERENCES `khach_hang` (`ma_khach_hang`);

--
-- Constraints for table `hoa_don_chi_tiet`
--
ALTER TABLE `hoa_don_chi_tiet`
  ADD CONSTRAINT `hoa_don_chi_tiet_ibfk_1` FOREIGN KEY (`ma_hoa_don`) REFERENCES `hoa_don` (`ma_hoa_don`),
  ADD CONSTRAINT `hoa_don_chi_tiet_ibfk_2` FOREIGN KEY (`ma_san_pham`) REFERENCES `san_pham` (`ma_san_pham`);

--
-- Constraints for table `san_pham`
--
ALTER TABLE `san_pham`
  ADD CONSTRAINT `san_pham_ibfk_1` FOREIGN KEY (`ma_nha_san_xuat`) REFERENCES `nha_san_xuat` (`ma_nha_san_xuat`),
  ADD CONSTRAINT `san_pham_ibfk_2` FOREIGN KEY (`ma_danh_muc`) REFERENCES `danh_muc` (`ma_danh_muc`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
